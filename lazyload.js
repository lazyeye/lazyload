// Requires
const path = require('path');
const fse = require('fs-extra');
const uuid = require('uuid/v4');
const Jimp = require('jimp')
const chokidar = require('chokidar');

// Classes
const lyyp = require('./lyyp');

/**
 * Manager for lazyload
 */
class Lazyload {

  /**
   * Initiates the Lazyload instance, fully preparing for use
   */
  async init() {

    /**
     * Houses all settings for lazyload. Loaded from local.json, stored in ./data
     */
    this.settings = {
      version: '1.1.0',
      perm: 'min',
      sort: false,
      asepriteExe: '',
      watchDir: '',
      yypPath: ``,
      prefix: '',
      stripSuffix: '_strip',
      casing: 'snake',
      origin: 'tl',
      scale: 1.0,
      projects: {}
    }

    /**
     * Whether or not lazyload is currently syncing files
     */
    this.sync = false;

    /**
     * Currently supported version of GMS2 (as an int)
     */
    this.acceptedGMVersion = 220258;

    /**
     * Lyyp instance of the current project
     */
    this.projectInstance = null;

    // Check GM Version
    try {
      const runtime = await fse.readJSON(path.join(process.env.PROGRAMDATA, 'GameMakerStudio2', 'runtime.json'));
      const userVersion = parseInt(runtime.active.replace(/\./g, ''));
      if (userVersion > this.acceptedGMVersion) {
        //TODO send ping to console
      }
    } catch (e) {
      // more pings
    }

    // Check local settings
    try {
      this.settings = await fse.readJSON('data', 'local.json');
    } catch (e) {
      // eula message
      this.saveLocalSettings();
    }

    // Load the project
    await this.loadProject();
  }

  /**
   * Creates and stores a lyyp instance of the set GM project
   */
  async loadProject() {
    if (this.settings.yypPath != null) {
      this.projectInstance = new lyyp.Project(this.settings.yypPath);
      await this.projectInstance.init(false);
    }
  }

  /**
   * Saves the current settings to ./data/local.json
   */
  saveLocalSettings() {
    const settingsContent = JSON.stringify(this.settings, null, 2);
    fse.writeFile(path.join('data', 'local.json'), settingsContent);
  }

  /**
   * Checks to see if the current set project has a cache in local.json. If it doesn't, it creates one
   */
  ensureProjectCache() {
    if (this.projectInstance != null) {
      const projectName = path.parse(this.projectInstance.yypPath).name;
      if (!(Object.keys(this.settings.projects).includes(projectName))) {
        this.settings.projects[projectName] = {};
        saveLocalSettings();
      }
    }
  }

  /**
   * Updates a property in the settings object, automatically saving
   * @param {string} setting The target property to change
   * @param {*} value The value to set the property to
   */
  async updateSettings(setting, value) {
    this.settings[setting] = value;
    this.saveLocalSettings();
  }

  /**
   * Disables syncing, or enables syncing if neccesary settings are set correctly.
   * Additionally ensures that chokidar is running
   * @param {boolean} flag Value to set syncing to
   */
  setSync(flag) {
    this.sync = flag;
    if (this.sync) {
      if (this.settings.yypPath != null
      && this.settings.yypPath != this.settings[watchDir]
      && this.settings.watchDir != null 
      && this.baseViewFile != null) {
        if (this.watcher == null) {
          this.watcher = chokidar.watch(this.settings.watchDir)
          this.watcher
            .on('add', dir => console.log(`${dir} added.`))
            .on('addDir', dir => console.log(`${dir} added.`))
            .on('change', dir => console.log(`${dir} updated.`))
            .on('unlink', dir => console.log(`${dir} removed.`));
        }
        // ping enabled syncing
      } else {
        // ping errored
        this.sync = false;
      }
    }
  }

  /**
   * Determines the needed view structure needed for a sprite, and ensures the views exist
   * @param {string} dir Directory to parse to determine view 
   */
  async handleSpriteView(dir) {

    /**
     * Searches the given view for the given resource
     * @param {string} resourceName Name of the resource to find
     * @param {string} viewID GUID of the view to search for the resource in
     */
    function findView(resourceName, viewID) {
      const parentView = this.projectInstance.resourceInfo[viewID];
      for (const childGuid of parentView.yy.children) {
        const childView = this.projectInstance.resourceInfo[childGuid];
        if (childView.name == resourceName) {
          return childGuid;
        }
      }
      return null;
    }

    // Construct needed views
    const dirArray = ['sprites'].concat(dir.replace(this.settings.watchDir, '').split(path.sep));
    let parentViewID = this.projectInstance.baseView;
    for (const dirName of dirArray) {
      viewID = findView(dirName, parentViewID);
      if (viewID == null) {
        const newView = new lyyp.GMView(dirName, 'GMSprite');
        this.projectInstance.injectNew(newView, parentViewID);
        viewID = newView.id;
      parentViewID = viewID;
      }
    }
    return parentViewID;
  }

  /**
   * Injects a new sprite to a project, handling naming and special settings
   * @param {string} imagePath Path to the image file to be injected
   */
  async injectSprite(imagePath) {

    /**
     * Converts the given input to snake_case or camelCase
     * @param {string} inputName Name to convert into resource name
     * @param {string} casing Casing to convert the name to -- 'snake' or 'camel'
     */
    function convertName(inputName, casing) {
      let output = inputName.replace(/\.(.*)/g, '');
      if (casing == 'snake') {
        output = output 
        .toLowerCase()
        .replace(/\s|-(?!$)/g, '_');
      } else {
        output = output 
          .toLowerCase()
          .split(/-|\s|_/g)
          .map((x, i) => i === 0 ? x : x[0].toUpperCase() + x.substr(1))
          .join('');
      }
      return output;
    }

    // Import image
    if (path.extname(imagePath) == '.png') {

      // Check for strip image
      const imageName = path.basename(imagePath);
      let frameCount = 1;
      if (imageName.slice(0, -1).endsWith(this.settings.stripSuffix)) {
        frameCount = int(imageName.slice(-1));
      }

      // Construct resource name
      const resourceName = convertName(path.basename(imagePath), this.settings.casing);

      // Create images
      let originalWidth, originalHeight;
      const jimpInstance = new Jimp(imagePath, (e, image) => {
        originalWidth = image.bitmap.width;
        originalHeight = image.bitmap.height;
      });
      const width = originalWidth / frameCount;
      const height = originalHeight / frameCount;
      const newSprite = new lyyp.GMSprite(resourceName, width, height);
      for (let i; i <= frameCount; i++) {
        
      }

      // Inject images
      newSprite.addFrames([jimpInstance]);
      this.projectInstance.injectNew(newSprite);

      // Log to the local data
      const projectName = path.parse(this.projectInstance.yypPath).name;
      this.settings.projects[projectName][imagePath.replace(this.watchDir)] = Date.now();
    }
  }
  
}

async function main() {
  const lazyload = new Lazyload();
  await lazyload.init();
  await lazyload.injectSprite('C:\\Users\\Gabe Weiner\\Desktop\\test.gif');
  await lazyload.projectInstance.commit();
}

main();
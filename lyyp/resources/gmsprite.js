const fse = require('fs-extra');
const uuid = require('uuid/v4');
const path = require('path');
const Jimp = require('jimp');

class GMSprite {

  /**
   * Creates a new, modifiable sprite resource
   * @param {string} resourceName The name of the sprite
   * @param {int} [imageWidth] The width for the sprite
   * @param {int} [imageHeight] THe height of the image 
   */
  constructor(resourceName, imageWidth=64, imageHeight=64) {
    this.id = uuid();
    this.name = resourceName;
    this.type = 'GMSprite';
    this.folderName = 'sprites';
    this.images = [];
    this.task = {
      'layers': {},

    };
    this._yy = {
      id: this.id,
      modelName: 'GMSprite',
      mvc: '1.12',
      name: this.name,
      For3D: false,
      HTile: false,
      VTile: false,
      bbox_bottom: 0,
      bbox_left: 0,
      bbox_right: 0,
      bbox_top: 0,
      bboxmode: 1,
      colkind: 1,
      coltolerance: 0,
      edgeFiltering: false,
      frames: [],
      gridX: 0,
      gridY: 0,
      height: imageHeight,
      layers: [
        {
          id: uuid(),
          modelName: 'GMImageLayer',
          mvc: '1.0',
          SpriteId: this.id,
          blendMode: 0,
          isLocked: false,
          name: 'default',
          opacity: 100,
          visible: true
        }
      ],
      origin: 0,
      originLocked: false,
      playbackSpeed: 15,
      playbackSpeedType: 0,
      premultiplyAlpha: false,
      sepmasks: false,
      swatchColours: null,
      swfPrecision: 2.525,
      textureGroupId: '1225f6b0-ac20-43bd-a82e-be73fa0b6f4f', // this is always the default texture page!
      type: 0,
      width: imageWidth,
      xorig: 0,
      yorig: 0
    }

    this.frameTemp = {
      id: 'uuid',
      modelName: 'GMSpriteFrame',
      mvc: '1.0',
      SpriteId: this._yy.id,
      compositeImage: {
          id: 'uuid',
          modelName: 'GMSpriteImage',
          mvc: '1.0',
          FrameId: 'uuid',
          LayerId: '00000000-0000-0000-0000-000000000000'
      },
      images: []
    }

    this.imageTemp = {
      id: 'uuid',
      modeName: 'GMSpriteImage',
      mvc: '1.0',
      FrameId: 'uuid',
      LayerId: 'uuid'
    }
  }

  /**
   * Adds given image(s) to the sprite
   * @param {Array} imageArray Collection of file paths OR jimp instances to be added to the sprite
   */
  addFrames(imageArray) {
    for (const thisImage of imageArray) {

      // Create frame
      let frame = { ...this.frameTemp };
      frame.id = uuid();
      frame.compositeImage.id = uuid();
      frame.compositeImage.FrameId = frame.id;
      frame.compositeImage.LayerId = '00000000-0000-0000-0000-000000000000'; 

      // Create image
      let image = { ...this.imageTemp }
      image.id = uuid();
      image.FrameId = frame.id;
      image.LayerId = this._yy.layers[0].id; // this is where layer support would go... if we had any
      frame.images.push(image);

      // Inject data
      this._yy.frames.push(frame);

      // Push image into class array
      this.images.push(thisImage);
    }
  }
}

// Export the class
module.exports = GMSprite;
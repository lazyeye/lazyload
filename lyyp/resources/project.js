// Requires
const path = require('path');
const fse = require('fs-extra');
const uuid = require('uuid/v4');
const ohm = require('ohm-js');
const Jimp = require('jimp');

/**
   * @desc Manager for GameMaker Studio 2 projects, creating an internal data model of the given project path,
   * and allowing for full, safe manipulation.
   */
class Project {

  //#region Methods
  constructor(yypPath) {

    /**
     * Checks for a match between a variable name and a resource name
     * @param {string} varName Name of the variable to cross check for resource
     */
    const handleDependency = (function (varName) {
      if (this.resourceNames.includes(varName)) {
        for (const guid of Object.keys(this._resourceInfo)) {
          const resource = this._resourceInfo[guid];
          if (varName == resource.name) {
            this.dependencyHolder.push(guid);
            break;
          }
        }
      }
    }).bind(this);

    // Class variables

    /**
     * Path to the yyp file
     * @type {string}
     */
    this._yypPath = yypPath;

    /**
     * Raw JSON data from the yyp file
     * @type {object}
     */
    this.yypRaw = JSON.parse(fse.readFileSync(this._yypPath, 'utf8'));

    /**
     * Path to the project
     * @type {string}
     */
    this._projectPath = path.dirname(this.yypPath);

    /**
     * GUID of the root view of the project
     * @type {string}
     */
    this.baseView = '';

    /**
     * All resource names in YYP
     * @type {Array}
     */
    this.resourceNames = [];

    /**
     * Full dictionary of resource information
     * @type {object}
     * @property {string} id Unique GUID for resource
     * @property {string} name Resource display name
     * @property {string} type Resource type
     * @property {string[]} dependencies GUID's of resources this resource is dependent upon
     * @property {string} parentView GUID of the view this resource appears in
     * @property {object} yy Full yy contents of the resource
     */
    this._resourceInfo = {};

    /**
     * Resources that have been modified since last commit
     * @type {Array}
     */
    this.modifiedResources = [];

    /**
     * Resources that are newly added and need their files generated
     * @type {Array}
     */
    this.newResources = [];

    /**
     * Middleman between operations and methods to track resource dependencies
     * (Hopefully can be removed by swapping operations with attributes)
     * @type {Array}
     */
    this.dependencyHolder = [];

    /**
     * Grammar object for gmlGrammar
     * @type {object}
     */
    this.grammar = ohm.grammar(fse.readFileSync('c:\\code\\lyyp\\utility\\gmlGrammar.ohm', 'utf8'));

    /**
     * Semantics object for gmlGrammar
     */
    this.semantics = this.grammar.createSemantics();
    this.semantics.addOperation('findResources', {
      possibleVariable: (variable) => {
        const varName = variable.sourceString;
        handleDependency(varName);
      },

      variable: (variable) => {
        const varName = variable.sourceString;
        handleDependency(varName);
      },

      funcIdentifier: (func) => {
        handleDependency(func);
      },

      // Generic for all non-terminal nodes
      _nonterminal: (children) => {
        children.forEach((element) => {
          element.findResources();
        });
      }, 

      // Generic for Termins:
      _terminal: () => {}
    });
  }

  /**
   * Constructs the base data for the YYP
   * @param {boolean} checkDependencies Whether or not the init method will scan every resource for dependencies.
   * Disabling dependency checking will make the operation much faster.
   */
  async init(checkDependencies) {

    /**
     * Seeks the parent view of a resource
     * @param {string} resourceID GUID of the resource
     * @returns {string} GUID of the parent view
     */
    function seekResource(resourceID) {
      for (const view of Object.keys(viewChildren)) {
        if (viewChildren[view].includes(resourceID)) {
          return path.parse(view).name;
        }
      }
      return 'root';
    }

    /**
     * Recurses through the layers object and returns a flattened array of all layer objects
     * @param {Array} layers 
     */
    function flattenLayers(layers) {
      let masterArray = [];
      for (const layer of layers) {
        if (layer.modelName == 'GMRLayer') {
          masterArray = masterArray.concat(flattenLayers(layer.layers));
        } else {
          masterArray.push(layer);
        }
      }
      return masterArray;
    }

    // Method variables
    let viewChildren = {};
    const viewNames = await fse.readdir(path.join(this._projectPath, 'views'));
    for (const view of viewNames) {
      const viewContent = await fse.readFile(path.join(this._projectPath, 'views', view));
      const viewRaw = JSON.parse(viewContent);
      viewChildren[view] = viewRaw.children;
    }

    // Construct data
    for (const resource of this.yypRaw.resources) {

      // Data
      const resourceID = resource.Key;
      const resourceType = resource.Value.resourceType;
      const resourcePath = resource.Value.resourcePath;
      const resourceContent = await fse.readFile(path.join(this._projectPath, resourcePath), 'utf8');
      const resourceYY = JSON.parse(resourceContent);
      let resourceName;
      if (resource.Value.resourceType == 'GMFolder') {
        resourceName = resourceYY.folderName;
      } else {
        resourceName = resourceYY.name;
        this.resourceNames.push(resourceName); // push to the names array since it is unique
      }

      // Now, we check the dependencies for the resource
      if (checkDependencies) {
        let gmlContent;
        let resourceFiles;
        switch (resourceType) {
    
          case 'GMScript':
    
            // Check for semantic dependencies
            gmlContent = await fse.readFile(path.join(this._projectPath, resourcePath).replace('.yy', '.gml'), 'utf8');
            try {
              const match = this.grammar.match(gmlContent);
              this.semantics(match).findResources();
            } catch {
              console.log(`There was an error attempting to read ${resourceName}`);
            }
          break;
          
          case 'GMObject':
    
            // Check for direct dependencies
            const properties = ['parentObjectId', 'spriteId', 'maskSpriteId'];
            for (const property of properties) {
              if (resourceYY[property] != '00000000-0000-0000-0000-000000000000') {
                this.dependencyHolder.push(resourceYY[property]);
              }
            }
            for (const event of resourceYY.eventList) {
              if (event.collisionObjectId != '00000000-0000-0000-0000-000000000000') {
                this.dependencyHolder.push(event.collisionObjectId);
              }
            }
    
            // Check for semantic dependencies
            resourceFiles = await fse.readdir(path.join(this._projectPath, path.dirname(resourcePath)));
            for (const file of resourceFiles) {
              if (path.extname(file) == '.gml') {
                gmlContent = await fse.readFile(path.join(this._projectPath, path.dirname(resourcePath), file), 'utf8');
                try {
                  const match = this.grammar.match(gmlContent);
                  this.semantics(match).findResources();
                } catch {
                  console.log(`There was an error attempting to read ${resourceName}, ${file}`);
                }
              }
            }

          break;

          case 'GMRoom':

            // Check for direct dependencies
            if (resourceYY.parentId != '00000000-0000-0000-0000-000000000000') {
              this.dependencyHolder.push(resourceYY.parentId);
            }
            let layers = flattenLayers(resourceYY.layers);
            for (const layer of layers) {
              switch (layer.modelName) {
                case 'GMRTileLayer':

                  // Check if there is a tileset attached to this layer
                  if (layer.tilesetId != '00000000-0000-0000-0000-000000000000') {
                    this.dependencyHolder.push(layer.tilesetId);
                  }
                
                break;

                case 'GMRInstanceLayer':

                  // Add all instances
                  for (const inst of layer.instances) {
                    this.dependencyHolder.push(inst.id);
                  }

                break;

                case 'GMRBackgroundLayer':

                  // Check if there is a sprite attached to this layer
                  if (layer.spriteId != '00000000-0000-0000-0000-000000000000') {
                    this.dependencyHolder.push(layer.spriteId);
                  }

                break;

                case 'GMRPathLayer':

                // Check if there is a path  attached to this layer
                if (layer.pathId != '00000000-0000-0000-0000-000000000000') {
                  this.dependencyHolder.push(layer.pathId);
                }

                break;
              }
            }

            // Check for semantic dependencies
            resourceFiles = await fse.readdir(path.join(this._projectPath, path.dirname(resourcePath)));
            for (const file of resourceFiles) {
              if (path.extname(file) == '.gml') {
                gmlContent = await fse.readFile(path.join(this._projectPath, path.dirname(resourcePath), file), 'utf8');
                try {
                  const match = this.grammar.match(gmlContent);
                  this.semantics(match).findResources();
                } catch {
                  console.log(`There was an error attempting to read ${resourceName}, ${file}`);
                }
              }
            }

          break;

        }
      }

      // Build entry to resourceInfo
      // Not using setResource because we don't want this marked as a modification!
      this._resourceInfo[resourceID] = {
        type: resourceType,
        path: resourcePath,
        name: resourceName,
        dependencies: this.dependencyHolder,
        parentView: seekResource(resourceID),
        yy: resourceYY
      };

      // Clear the dependency holder
      this.dependencyHolder = [];
    }

    // Find the base view
    for (const view of viewNames) {
      const viewContent = await fse.readFile(path.join(this._projectPath, 'views', view));
      const viewRaw = JSON.parse(viewContent);
      if (viewRaw.isDefaultView) {
        this.baseView = path.parse(view).name;
        break;
      }
    }
  }

  /**
   * Sets a new object for the given resource, creating a new entry if needed
   * @param {string} guid The ID of the resource to modify
   * @param {object} resource The object to replace the current resourceInfo value
   */
  async setResource(guid, resource) {

    // Log resource accordingly
    if (!(this.resourceNames.includes(resource.name))) {
      this.resourceNames.push(resource.name);
    }
    if (!(this.modifiedResources.includes(guid))) {
      this.modifiedResources.push(guid);
    }

    // Update YY of the resource
    if (resource.type == 'GMFolder') {
      resource.yy.folderName = resource.name;
    } else {
      resource.yy.name = resource.name;
    }
    
    // Commit changes
    this._resourceInfo[guid] = resource;
  }

  /**
   * Constructs and returns a resource tree based on resourceInfo
   */
  async constructResourceTree() {

    /**
    * Recurses through view file, returning an object with its contents
    * @param {string} viewID
    */
    const readView = (async function (viewID) {

      // Create object for view
      let view = {};

      // Traverse the view
      for (const child of this._resourceInfo[viewID].yy.children) {

        // Log the resource in the view
        if (this._resourceInfo[child].type != 'GMFolder') {
          view[this._resourceInfo[child].name] = { id: child, children: null };
        } else {
          view[this._resourceInfo[child].name] = { id: child, children: await readView(child) };
        }
      }
      return view;
    }).bind(this);

    // Return the resource tree
    return await readView(this.baseView);
  }
  /**
    * Creates a resource injection for yyp's
    * @param {string} resourceID GUID of the resource
    * @param {string} resourceType GM Model of the resource
    * @param {string} resourcePath Relative path to the resource
    * @returns {object} Injection for the YYP
    */
   constructInjection(resourceID, resourceType, resourcePath) {
    return {
      Key: resourceID,
      Value: {
          id: uuid(),
          modelName: 'GMResourceInfo',
          mvc: '1.0',
          configDeltaFiles: [],
          configDeltas: [],
          resourceCreationConfigs: ['default'],
          resourcePath: resourcePath,
          resourceType: resourceType
      }
    };
  }

  /**
   * Injects an array of resources from the given source yyp
   * @param {Object} sourceYYP The YYP that the resource originates from
   * @param {string[]} guidArray GUID's of all resources to be imported
   */
  async injectFromYYP(sourceYYP, guidArray) {
    for (const guid of guidArray) {

      // Fetch resource information
      const resource = sourceYYP._resourceInfo[guid];

      // Prevent naming conflicts
      if (this.resourceNames.includes(resource.name)) {
        console.log(`The target yyp already contains a resource called ${resource.name}!`);
        continue;
      }

      // Copy files
      await fse.copy(path.join(sourceYYP._projectPath, resource.path), path.join(this._projectPath, resource.path));

      // Create injections
      this.yypRaw.resources.push(this.constructInjection(resource.id, resource.type, resource.path));

      // Update internal data
      this.resourceNames.push(resource.name);
      this.setResource(guid, {
        type: resource.type,
        path: resource.path,
        name: resource.name,
        dependencies: resource.dependencies,
        parentView: resource.parentView
      });
    }
  }

  /**
   * 
   * @param {Object} resourceInstance The instance of the new resource
   * @param {string} parentView GUID of the view to inject resource to. If none is provided,
   * it defaults to the base view of the given resource type.
   */
  injectNew(resourceInstance, parentView=null) {

    // Create resource object
    const yy = resourceInstance._yy;
    const resource = {
      type: resourceInstance.type,
      path: path.join(resourceInstance.folderName, resourceInstance.name, `${resourceInstance.name}.yy`),
      name: resourceInstance.name,
      dependencies: [],
      parentView: parentView,
      yy: yy
    }

    // Handle default view
    if (resource.parentView == null) {
      const guidArray = Object.keys(this._resourceInfo).filter(guid => this._resourceInfo[guid].type == 'GMFolder');
      for (const guid of guidArray) {
        const view = this._resourceInfo[guid];
        if (view.parentView == this.baseView && view.name == resourceInstance.folderName) {
          resource.parentView = guid;
          break;
        }
      }
    }
    
    // Append to view
    let view = this._resourceInfo[resource.parentView];
    view.yy.children.push(yy.id);
    this.setResource(resource.parentView, view);

    // Create injections
    this.yypRaw.resources.push(this.constructInjection(resourceInstance.id, resource.type, resource.path));

    // Update internal data
    this.newResources.push(resourceInstance);
    this.resourceNames.push(resource.name);
    // Not using setResource because we don't want this marked as a modification! #TODO fix this bullshit
    this._resourceInfo[resource.id] = resource;
  }

  /**
   * 
   * Sorts the resource tree for the given views
   * @param {string[]} viewNameArray The names of the default views to sort. If none are provided, the whole project is sorted.
   */
  async sortResourceTree(viewNameArray = [
    'sprites',
    'tilesets',
    'sounds',
    'paths',
    'scripts',
    'shaders',
    'fonts',
    'timelines',
    'objects',
    'rooms',
    'notes',
    'includedfiles',
    'extentions',
    'options',
    'configurations'
    ]) {

    /**
     * Sorts the given view object's children recursively
     * @returns {object} New view object
     */
    const sortView = ((view) => {

      /**
       * Returns the given object sorted alphabetically
       * @param {object} obj Object to sort alphabetically
       * @returns {object} Inputted object sorted 
       */
      const sortObject = (obj) => {
        let sortedObj = {};
        Object.keys(obj).sort().forEach((key) => {
          sortedObj[key] = obj[key];
        });
        return sortedObj;
      }

      // Sort the view
      const childResources = {};
      const childViews = {};
      for (const childID of view.yy.children) {
        const child = this._resourceInfo[childID];

        // Weird bullshit with altered resources or whatever, #TODO fix
        if (child == undefined) {
          continue;
        }
        if (child.type == 'GMFolder') {
          childViews[child.name] = childID;
          this.setResource(childID, sortView(child));
        } else {
          childResources[child.name] = childID;
        }
      }

      // Update resource info
      const newChildren = {...sortObject(childViews), ...sortObject(childResources) };
      view.yy.children = Object.values(newChildren);
      return view;
    }).bind(this);

    // Parse all supplied views to be sorted
    for (const viewName of viewNameArray) {
      
      // Find the view
      for (const guid of Object.keys(this._resourceInfo)) {
        const resource = this._resourceInfo[guid];

        // Identify potential match
        if (resource.type == 'GMFolder' && resource.name == viewName) {

          // Confirm match
          if (resource.parentView == this.baseView || resource.parentView == 'root') {

            // Sort it out
            this.setResource(guid, sortView(resource));
          }
        }
      }
    }
  }

  async commit() {

    // Create new resources
    for (const resourceInstance of this.newResources) {
      switch (resourceInstance.type) {
        case 'GMSprite':

          // Create needed directories
          const resourcePath = path.join(this.projectPath, 'sprites', resourceInstance.name);
          await fse.ensureDir(path.join(resourcePath, 'layers'));

          // Create images
          let i = 0;
          for (const image of resourceInstance.images) {
            const layerID = resourceInstance._yy.layers[0].id; // this is ALSO where layer support would go... you get the idea
            const frameID = resourceInstance._yy.frames[i++].id;
            await fse.ensureDir(path.join(resourcePath, 'layers', frameID));
            if (typeof(image) == 'string') {
              await fse.copyFile(image, path.join(resourcePath, `${frameID}.png`));
              await fse.copyFile(image, path.join(resourcePath, 'layers', frameID, `${layerID}.png`));
            } else if (image instanceof Jimp) {
              await image.write(path.join(resourcePath, `${frameID}.png`));
              await image.write(path.join(resourcePath, 'layers', frameID, `${layerID}.png`));
            }
          }

          // Create yy
          const yyContent = JSON.stringify(resourceInstance._yy, null, 4);
          await fse.writeFile(path.join(resourcePath, `${resourceInstance.name}.yy`), yyContent, 'utf8');
        
        break;
      }
    }

    // Update modified resources
    for (const guid of this.modifiedResources) {
      const resource = this._resourceInfo[guid];
      const newContent = JSON.stringify(resource.yy, null, 4);
      await fse.writeFile(path.join(this._projectPath, resource.path), newContent);
    }

    // Update yyp
    const newYYP = JSON.stringify(this.yypRaw, null, 4);
    await fse.writeFile(this.yypPath, newYYP);
  }
  //#endregion

  // #region Getters and Setters

  get resourceInfo() {
    return this._resourceInfo;
  }

  get yypPath() {
    return this._yypPath;
  }

  get projectPath() {
    return this._projectPath;
  }

  //#endregion
}
module.exports = Project;